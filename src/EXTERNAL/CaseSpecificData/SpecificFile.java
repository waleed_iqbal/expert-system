package EXTERNAL.CaseSpecificData;

import EXTERNAL.KnowledgeBase.Detail;
import EXTERNAL.KnowledgeBase.Queries;
import SHELL.InferenceEngine.InferenceEngine;
import java.sql.SQLException;
import java.util.ArrayList;

public class SpecificFile {

    private ArrayList<Boolean> userSymptoms;
    private ArrayList<String> sym=new ArrayList<String>();
    private ArrayList<Detail> DD=new ArrayList<Detail>();



    public SpecificFile(ArrayList<Boolean> userSym) throws ClassNotFoundException, SQLException {

        this.userSymptoms=userSym;
        patSym();
        detailing();
        InferenceEngine IE=new InferenceEngine(this.sym,this.DD);
    }

    public ArrayList<String> getSymFromDataBase() throws ClassNotFoundException, SQLException
    {
        ArrayList<String> arr= new ArrayList<String>();
        Queries q = new Queries();
        arr = q.getSymptoms();

        return arr;
    }

    public void patSym() throws ClassNotFoundException, SQLException
    {
        ArrayList<String> temp=new ArrayList<String>();
        temp = getSymFromDataBase();


        for(int i=0;i<temp.size();i++)
        {
            if(this.userSymptoms.get(i).booleanValue())
            {
                this.sym.add(temp.get(i).toString());
            }

        }
    }

    public void detailing() throws ClassNotFoundException, SQLException
    {

        Queries Q=new Queries();
        ArrayList<String> dis=new ArrayList<String>();
        dis=Q.getDiseses();
        for(int i=0;i<dis.size();i++)
        {
            ArrayList<String> sym = new ArrayList<String>();
            sym = Q.getSymFromId(i+1);
            Detail temp=new Detail(sym,dis.get(i));
            DD.add(temp);
        }
    }
}
