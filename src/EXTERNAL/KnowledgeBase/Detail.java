
package EXTERNAL.KnowledgeBase;

import java.util.ArrayList;

public class Detail {

    private ArrayList<String> symptoms;
    private String disease;

    public Detail(ArrayList<String> symptoms, String disease) {
        this.symptoms = symptoms;
        this.disease = disease;
    }

    public String getDisease() {
        return disease;
    }

    public ArrayList<String> getSymptoms() {
        return symptoms;
    }
}
