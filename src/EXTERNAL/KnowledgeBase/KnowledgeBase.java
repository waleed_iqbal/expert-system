
package EXTERNAL.KnowledgeBase;

import java.util.ArrayList;

public class KnowledgeBase {

    private ArrayList<String> symptoms = new ArrayList<String>();

    public ArrayList<String> getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(ArrayList<String> symptoms) {
        this.symptoms = symptoms;
    }

    public KnowledgeBase(ArrayList<String> userSymptoms) {

        this.symptoms=userSymptoms;
    }

 
}