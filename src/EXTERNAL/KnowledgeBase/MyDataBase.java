package EXTERNAL.KnowledgeBase;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MyDataBase {
     
     private static Connection conn;
     private ResultSet res;
     private Statement stat;


     public Connection makeConn() throws ClassNotFoundException, SQLException
     {
         Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
	 conn = DriverManager.getConnection("jdbc:odbc:kb_db");
         return conn;
     }
     
     public void closeconn() throws SQLException
     {
         conn.close();
     }

      public ResultSet RunSql(String sql) throws ClassNotFoundException, SQLException
      {                          
         conn=makeConn();
         stat=conn.createStatement();
         res=stat.executeQuery(sql);
         return res;
       }
}