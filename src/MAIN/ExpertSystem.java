package MAIN;

import SHELL.UserInterface.IndexPage;
import javax.swing.*;

public class ExpertSystem {

    public static void main(String[] args) {

        JFrame board=new JFrame("EXPERT SYSTEM");
        IndexPage IP=new IndexPage(board);
        board.setResizable(false);
        board.setVisible(true);
        board.setExtendedState(board.getExtendedState()|JFrame.MAXIMIZED_BOTH);
        board.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
