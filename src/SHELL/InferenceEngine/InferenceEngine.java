package SHELL.InferenceEngine;

import EXTERNAL.KnowledgeBase.Detail;
import SHELL.ExplanationSystem.ExplanationSystem;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class InferenceEngine {
    
    private ArrayList<String> dis_name = new ArrayList<String>();
    private ArrayList<Double> percent = new ArrayList<Double>();
    private String finalDis;
    private double per;
    private ArrayList<String> UserSymptoms=new ArrayList<String>();

    int c;

    public InferenceEngine(ArrayList<String> input1, ArrayList<Detail> input2) throws ClassNotFoundException, SQLException
    {
        UserSymptoms=input1;
        matchDiseases(input1, input2);
    }

    public void matchDiseases(ArrayList<String> userSym, ArrayList<Detail> detail) throws ClassNotFoundException, SQLException
    {
        double count=0.0;
        for(int i=0; i<detail.size(); i++)
        {
            count=0.0;
            for(int j=0; j<userSym.size(); j++)
            {   
                 for(int k=0;k<detail.get(i).getSymptoms().size();k++)
                 {
                     if(userSym.get(j).equalsIgnoreCase(detail.get(i).getSymptoms().get(k).toString()))
                         count++;
                 }
            }
            dis_name.add(detail.get(i).getDisease().toString());
            double temp = count/detail.get(i).getSymptoms().size();
            double cal = (temp*100)+detail.get(i).getSymptoms().size()+((detail.get(i).getSymptoms().size()-count)*10);
            percent.add(cal);
        }
        finalDis();
    }

    public void finalDis() throws ClassNotFoundException, SQLException
    {
          for(int i=0;i<dis_name.size();i++)
          {
              if(percent.get(i).doubleValue()>per)
              {
                  per=percent.get(i).doubleValue();
                  finalDis=dis_name.get(i).toString();
                  this.c=i;
              }
          }
          ExplanationSystem ES=new ExplanationSystem();
          ES.Display(UserSymptoms,per,finalDis,c+1);
    }
}
