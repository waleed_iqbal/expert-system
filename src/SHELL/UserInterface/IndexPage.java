package SHELL.UserInterface;

import EXTERNAL.KnowledgeBase.Queries;
import java.awt.BorderLayout;
import java.awt.Color;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

public class IndexPage extends javax.swing.JPanel {

    private JFrame frame;

    public IndexPage(JFrame fr) {
        this.frame=fr;
        initComponents();
        this.frame.add(this);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        Bio = new javax.swing.JButton();
        Intero = new javax.swing.JButton();
        Medicine = new javax.swing.JButton();
        Clear = new javax.swing.JButton();
        Disease = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        InnerPanel = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        help = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SHELL/UserInterface/header.jpg"))); // NOI18N

        jLabel2.setBackground(new java.awt.Color(153, 153, 255));
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SHELL/UserInterface/img.jpg"))); // NOI18N

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        Bio.setBackground(new java.awt.Color(255, 255, 255));
        Bio.setText("NAME & AGE");
        Bio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BioActionPerformed(evt);
            }
        });

        Intero.setBackground(new java.awt.Color(255, 255, 255));
        Intero.setText("INTEROGATION");
        Intero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                InteroActionPerformed(evt);
            }
        });

        Medicine.setBackground(new java.awt.Color(255, 255, 255));
        Medicine.setText("MEDICINES");
        Medicine.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MedicineActionPerformed(evt);
            }
        });

        Clear.setBackground(new java.awt.Color(255, 255, 255));
        Clear.setText("CLEAR PATIENT");
        Clear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ClearActionPerformed(evt);
            }
        });

        Disease.setBackground(new java.awt.Color(255, 255, 255));
        Disease.setText("DISEASES");
        Disease.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DiseaseActionPerformed(evt);
            }
        });

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SHELL/UserInterface/line.png"))); // NOI18N

        InnerPanel.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout InnerPanelLayout = new javax.swing.GroupLayout(InnerPanel);
        InnerPanel.setLayout(InnerPanelLayout);
        InnerPanelLayout.setHorizontalGroup(
            InnerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 669, Short.MAX_VALUE)
        );
        InnerPanelLayout.setVerticalGroup(
            InnerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 333, Short.MAX_VALUE)
        );

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SHELL/UserInterface/line.png"))); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(91, 91, 91)
                .addComponent(Bio, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Intero, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Medicine, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Disease, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Clear, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(89, 89, 89))
            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 839, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(74, 74, 74)
                .addComponent(InnerPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(96, Short.MAX_VALUE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 809, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(20, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Medicine, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Disease, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Clear, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Intero, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Bio, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(InnerPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel5)
                .addContainerGap(21, Short.MAX_VALUE))
        );

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SHELL/UserInterface/footer.png"))); // NOI18N

        help.setBackground(new java.awt.Color(255, 255, 255));
        help.setText("HELP");
        help.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                helpMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                helpMouseExited(evt);
            }
        });
        help.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                helpActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 456, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(help)
                        .addGap(21, 21, 21))))
            .addComponent(jLabel1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 233, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel2)
                        .addGap(26, 26, 26)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(17, 17, 17)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(help)))
                .addGap(35, 35, 35))
        );
    }// </editor-fold>//GEN-END:initComponents

private void BioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BioActionPerformed
    Bio.setBackground(Color.LIGHT_GRAY);
    Clear.setBackground(Color.WHITE);
    Intero.setBackground(Color.WHITE);
    Medicine.setBackground(Color.WHITE);
    Disease.setBackground(Color.WHITE);
    
    /***************************/
        InnerPanel.removeAll();
        InnerPanel.setLayout(new BorderLayout());
        Initial AV=new Initial();
        InnerPanel.add(AV,BorderLayout.CENTER);
        InnerPanel.setVisible(true);
        AV.setVisible(true);
        InnerPanel.validate();
        InnerPanel.repaint();
}//GEN-LAST:event_BioActionPerformed

private void ClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ClearActionPerformed
    Bio.setBackground(Color.WHITE);
    Clear.setBackground(Color.LIGHT_GRAY);
    Intero.setBackground(Color.WHITE);
    Medicine.setBackground(Color.WHITE);
    Disease.setBackground(Color.WHITE);
    
    /***************************/
        InnerPanel.removeAll();
        InnerPanel.setLayout(new BorderLayout());
        InnerPanel.setVisible(true);
        InnerPanel.validate();
        InnerPanel.repaint();
}//GEN-LAST:event_ClearActionPerformed

private void InteroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_InteroActionPerformed
    Bio.setBackground(Color.WHITE);
    Clear.setBackground(Color.WHITE);
    Intero.setBackground(Color.LIGHT_GRAY);
    Medicine.setBackground(Color.WHITE);
    Disease.setBackground(Color.WHITE);
    
    ArrayList<Boolean> answers = new ArrayList<Boolean>();
    
        ArrayList<String> Ques=new ArrayList<String>();
        Queries Q=new Queries();
        try {
            Ques = Q.GetQuestions();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(IndexPage.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(IndexPage.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    /***************************/
        InnerPanel.removeAll();
        InnerPanel.setLayout(new BorderLayout());
        Interogation AV=null;
        try {
            AV = new Interogation(Ques,0,answers);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(IndexPage.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(IndexPage.class.getName()).log(Level.SEVERE, null, ex);
        }
        InnerPanel.add(AV,BorderLayout.CENTER);
        InnerPanel.setVisible(true);
        AV.setVisible(true);
        InnerPanel.validate();
        InnerPanel.repaint();
}//GEN-LAST:event_InteroActionPerformed

private void MedicineActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MedicineActionPerformed
    Bio.setBackground(Color.WHITE);
    Clear.setBackground(Color.WHITE);
    Intero.setBackground(Color.WHITE);
    Medicine.setBackground(Color.LIGHT_GRAY);
    Disease.setBackground(Color.WHITE);
    String mess="THIS EXPERT SYSTEM KNOWS ABOUT FOLLOWING MEDICINES\n";
        ArrayList<String> dis=new ArrayList<String>();
    Queries Q=new Queries();
        try {
            dis=Q.getMedicines();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(IndexPage.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(IndexPage.class.getName()).log(Level.SEVERE, null, ex);
        }
    for(int i=0;i<dis.size();i++)
    {
        mess=mess+dis.get(i).toString()+"\n";
    }
    JOptionPane.showMessageDialog(null,mess);
}//GEN-LAST:event_MedicineActionPerformed

private void DiseaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DiseaseActionPerformed
    Bio.setBackground(Color.WHITE);
    Clear.setBackground(Color.WHITE);
    Intero.setBackground(Color.WHITE);
    Medicine.setBackground(Color.WHITE);
    Disease.setBackground(Color.LIGHT_GRAY);
    String mess="THIS EXPERT SYSTEM KNOWS ABOUT FOLLOWING DISEASES\n";
    ArrayList<String> dis=new ArrayList<String>();
    Queries Q=new Queries();
        try {
            dis=Q.getDiseses();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(IndexPage.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(IndexPage.class.getName()).log(Level.SEVERE, null, ex);
        }
    for(int i=0;i<dis.size();i++)
    {
        mess=mess+dis.get(i).toString()+"\n";
    }
    JOptionPane.showMessageDialog(null,mess);
}//GEN-LAST:event_DiseaseActionPerformed

private void helpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_helpActionPerformed
    JOptionPane.showMessageDialog(null,  "\n"
                                        + "1. FIRST CLICK ON THE NAME & AGE TAB AND GIVE THE REQUIRE INFORMATION. \n\n"
                                        +"2. THEN CLICK ON INTEROGATION TAB AND GIVE ANSWERS TO THE QUESTIONS. \n\n"
                                        +"3. AFTER INTEROGATION CLICK ON THE SUBMIT BUTTON. \n\n"
                                        +"4. AFTER THAT SYSTEM WILL SHOW YOU THE EXPLANATION. \n\n"
                                        +"5. WHEN YOU ARE DONE CLICK ON CLEAR BUTTON TO CLEAR WORKING MEMORY.\n\n");
}//GEN-LAST:event_helpActionPerformed

private void helpMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_helpMouseEntered
    help.setBackground(Color.LIGHT_GRAY);
    help.setForeground(Color.WHITE);
}//GEN-LAST:event_helpMouseEntered

private void helpMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_helpMouseExited
    help.setBackground(Color.WHITE);
    help.setForeground(Color.BLACK);
}//GEN-LAST:event_helpMouseExited


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Bio;
    private javax.swing.JButton Clear;
    private javax.swing.JButton Disease;
    private javax.swing.JPanel InnerPanel;
    private javax.swing.JButton Intero;
    private javax.swing.JButton Medicine;
    private javax.swing.JButton help;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel2;
    // End of variables declaration//GEN-END:variables

}
