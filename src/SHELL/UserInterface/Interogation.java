package SHELL.UserInterface;

import EXTERNAL.CaseSpecificData.SpecificFile;
import java.awt.BorderLayout;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class Interogation extends javax.swing.JPanel {

    

    public Interogation(ArrayList<String> Q,int cnt, ArrayList<Boolean> ans) throws ClassNotFoundException, SQLException {
        this.counter=cnt;
        this.ques=Q;
        this.ans=ans;
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Btn = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        y = new javax.swing.JRadioButton();
        n = new javax.swing.JRadioButton();

        setBackground(new java.awt.Color(255, 255, 255));

        Btn.setText("CONTINUE");
        Btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText(this.ques.get(this.counter).toString());

        y.setBackground(new java.awt.Color(255, 255, 255));
        y.setText("YES");
        y.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                yActionPerformed(evt);
            }
        });

        n.setBackground(new java.awt.Color(255, 255, 255));
        n.setText("NO");
        n.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 556, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Btn)
                            .addComponent(n)
                            .addComponent(y))
                        .addGap(59, 59, 59)))
                .addGap(14, 14, 14))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(y)
                .addGap(18, 18, 18)
                .addComponent(n)
                .addGap(18, 18, 18)
                .addComponent(Btn)
                .addContainerGap(138, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

private void BtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnActionPerformed


   if(n.isSelected())
       ans.add(Boolean.FALSE);
   else if(y.isSelected())
       ans.add(Boolean.TRUE);
   else
       ans.add(Boolean.FALSE);

    if(this.counter<ques.size()-1)
    {    this.counter++;
        this.removeAll();
        this.setLayout(new BorderLayout());
        Interogation AV=null;
        try {
            AV = new Interogation(this.ques,this.counter, this.ans);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(IndexPage.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(IndexPage.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.add(AV,BorderLayout.CENTER);
        this.setVisible(true);
        AV.setVisible(true);
        this.validate();
        this.repaint();
    }
    else
    {
       JOptionPane.showMessageDialog(null,"INTEROGATION IS OVER ");
         try {
                SpecificFile s = new SpecificFile(ans);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Interogation.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(Interogation.class.getName()).log(Level.SEVERE, null, ex);
            }
        this.removeAll();
        this.setLayout(new BorderLayout());
        this.setVisible(true);
        this.validate();
        this.repaint();
    }
    
}//GEN-LAST:event_BtnActionPerformed

private void yActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_yActionPerformed
    if(n.isSelected())
        n.setSelected(false);
}//GEN-LAST:event_yActionPerformed

private void nActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nActionPerformed
     if(y.isSelected())
        y.setSelected(false);
}//GEN-LAST:event_nActionPerformed


    private ArrayList<String> ques=new ArrayList<String>();
    private int counter;
    private ArrayList<Boolean> ans = new ArrayList<Boolean>();
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Btn;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JRadioButton n;
    private javax.swing.JRadioButton y;
    // End of variables declaration//GEN-END:variables
}
